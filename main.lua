local input   = require("input") -- LOAD CUSTOM INPUT LIBRARY
local check   = require("check") -- LOAD OPERATING SYSTEM CHECK LIBRARY
local execute = true
local cos     = check.os() -- CHECK THE CURRENT OPERATING SYSTEM
local command = ""
local VERSION = "ADB Shell 1.0" -- VERSION STRING
if cos == "windows" then
	color   = "0A"
end
local cmd     = "adb"
local inputTxt= "ADB"
--------- GNU/LINUX SETUP ----------
elseif cos == "linux" then
	os.execute("./adb start-server")
	os.execute("clear")
end
--------- WINDOWS SETUP --------
if cos == "windows" then
os.execute("@echo off")
os.execute("title "..VERSION)
os.execute("color "..color)
os.execute("adb start-server")
os.execute("cls")
-------------- POST ------------
print(VERSION)
print("Powered by ".._VERSION)
print("")

while execute == true do -- MAIN LOOP
	command = input.read(inputTxt)
	if command == "exit" then
		execute = false
	elseif command == "clear" then
		if cos == "windows" then
			os.execute("cls")
		elseif cos == "linux" then
			os.execute("clear")
		end
	elseif command == "fastboot" and cmd ~= "fastboot" then -- ADB TO FASTBOOT SWITCHER
		cmd = "fastboot"
		inputTxt = "FASTBOOT"
	elseif command == "adb" and cmd ~= "adb" then -- FASTBOOT TO ADB SWITCHER
		cmd = "adb"
		inputTxt = "ADB"
	else
		if cos == "windows" then -- WINDOWS MODE
			os.execute(cmd.." "..command)
		elseif cos == "linux" then -- GNU/LINUX MODE
			os.execute("./"..cmd.." "..command)
		end
	end
end

if cos == "windows" then -- KILL ADB SERVER IN WINDOWS
	os.execute("adb kill-server")
elseif cos == "linux" then -- KILL ADB SERVER IN GNU/LINUX
	os.execute("./adb kill-server")
end

os.exit()
