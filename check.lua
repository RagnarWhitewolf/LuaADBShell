check = {}

  function check.os()
    osd = package.config:sub(1,1)
    osr = "ERROR!"
    if osd == "/" then
        osr = "linux"
    elseif osd == "\\" then
        osr = "windows"
    end
    return osr
  end

return check
