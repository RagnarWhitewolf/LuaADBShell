# LuaADBShell
ADB shell writen in Lua

## INSTRUCTIONS:

In order to execute this shell you need to follow this steps:

1. Copy all the adb files in the root directory where the main.lua or the compiled file is located
2. Execute the main.lua script or compile it, if any error appears submit an issue in this repository.
